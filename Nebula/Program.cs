﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Nebula.Database;
using Serilog;
using Serilog.Events;

namespace Nebula
{
    public class Program
    {
        public static void Main(string[] args)
        {
            Log.Logger = new LoggerConfiguration()
                .MinimumLevel.Information()
                .MinimumLevel.Override("Microsoft", LogEventLevel.Warning)
                .Enrich.FromLogContext()
                .WriteTo.File("log.txt", rollOnFileSizeLimit: true, fileSizeLimitBytes: 20_971_520) // 20 MB log files
                //.WriteTo.InfluxDB()
                .CreateLogger();

            try
            {
                Log.Information("Starting host");

                var host = CreateWebHostBuilder(args).Build();
                host.InitializeDatabases().ConfigureAwait(false);
                host.Run();
            }
            catch (Exception ex)
            {
                Log.Fatal(ex, "Host terminated unexpectedly");
            }
            finally
            {
                Log.CloseAndFlush();
            }
        }

        public static IWebHostBuilder CreateWebHostBuilder(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                //.UseKestrel()
                .UseSerilog()
                .UseStartup<Startup>();
    }
}
