﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IdentityServer4.Events;
using IdentityServer4.Services;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Nebula.Database;

namespace Nebula.Services
{
    public class IdentityServerEventSink : IEventSink
    {
        private readonly ILogger<IdentityServerEventSink> _logger;
        private readonly AuthDbContext _dbContext;

        public IdentityServerEventSink(ILogger<IdentityServerEventSink> logger, AuthDbContext dbContext)
        {
            _logger = logger;
            _dbContext = dbContext;
        }

        public async Task PersistAsync(Event evt)
        {
            switch (evt.Id)
            {
                case EventIds.TokenIssuedSuccess:
                    await HandleTokenIssuedEvent(evt);
                    break;

                case EventIds.ApiAuthenticationFailure:
                case EventIds.ClientAuthenticationFailure:
                case EventIds.TokenIntrospectionFailure:
                    break;
            }
        }

        #region event handlers

        /// <summary>
        /// On every successful login, store the current time and the user's IP address
        /// </summary>
        private async Task HandleTokenIssuedEvent(Event evt)
        {
            try
            {
                if (!(evt is TokenIssuedSuccessEvent eventInfo)) return;

                _logger.LogInformation($"{nameof(HandleTokenIssuedEvent)} finding user with id = {eventInfo.SubjectId}");

                if (!int.TryParse(eventInfo.SubjectId, out var userId)) return;

                var user = await _dbContext.Users.FirstOrDefaultAsync(x => x.Id == userId);

                if (user == null)
                {
                    _logger.LogWarning($"{nameof(HandleTokenIssuedEvent)} couldn't find user with id = {userId}");

                    return;
                }

                _logger.LogInformation($"{nameof(HandleTokenIssuedEvent)} found user with id = {userId}");

                user.LastLogin = evt.TimeStamp;
                user.LastIpAddress = evt.RemoteIpAddress;

                _dbContext.Update(user);

                _logger.LogInformation($"{nameof(HandleTokenIssuedEvent)} updating user with id = {userId}");

                await _dbContext.SaveChangesAsync();

                _logger.LogInformation($"{nameof(HandleTokenIssuedEvent)} updated user with id = {userId}");
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, $"{nameof(HandleTokenIssuedEvent)} error");
            }
        }

        #endregion
    }
}
