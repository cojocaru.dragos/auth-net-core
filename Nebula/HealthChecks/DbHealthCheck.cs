﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Diagnostics.HealthChecks;
using Microsoft.Extensions.Logging;
using Nebula.Database;

namespace Nebula.HealthChecks
{
    public class DbHealthCheck : IHealthCheck
    {
        private readonly ILogger<DbHealthCheck> _logger;
        private readonly AuthDbContext _dbContext;

        public DbHealthCheck(ILogger<DbHealthCheck> logger, AuthDbContext dbContext)
        {
            _logger = logger;
            _dbContext = dbContext;
        }

        public async Task<HealthCheckResult> CheckHealthAsync(HealthCheckContext context, CancellationToken cancellationToken = default(CancellationToken))
        {
            try
            {
                await _dbContext.Database.ExecuteSqlCommandAsync("SELECT 1", cancellationToken: cancellationToken);

                return HealthCheckResult.Healthy("The check indicates a healthy result.");
            }
            catch (Exception ex)
            {
                _logger?.LogWarning($"The {nameof(DbHealthCheck)} check failed with the exception\n{ex.ToString()}\n{ex.InnerException?.ToString()}.");

                return HealthCheckResult.Unhealthy("The check indicates an unhealthy result.");
            }
        }
    }
}
