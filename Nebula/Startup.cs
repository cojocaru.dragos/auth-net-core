﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;
using IdentityModel;
using IdentityServer4.Services;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpOverrides;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Nebula.Database;
using Nebula.HealthChecks;
using Nebula.Services;

namespace Nebula
{
    public class Startup
    {
        private readonly IConfiguration _configuration;
        private readonly IHostingEnvironment _environment;
        private readonly ILogger<Startup> _logger;

        public Startup(IConfiguration configuration, IHostingEnvironment environment, ILogger<Startup> logger)
        {
            _configuration = configuration;
            _environment = environment;
            _logger = logger;
        }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);

            services.AddHealthChecks()
                .AddCheck<DbHealthCheck>("db_health_check");

            #region CORS configuration

            services.AddCors(options => options.AddPolicy("CorsPolicy",
                builder =>
                {
                    builder
                        .AllowAnyMethod()
                        .AllowAnyHeader()
                        .AllowAnyOrigin();
                }));

            #endregion

            var connectionString = _configuration.GetConnectionString("IdentityServer");
            if (string.IsNullOrEmpty(connectionString))
                _logger.LogCritical("No connection string was specified!");

            services.AddDbContext<AuthDbContext>(options => options.UseNpgsql(connectionString));

            services.AddIdentity<ApplicationUser, ApplicationRole>(options =>
                {
                    options.SignIn = new SignInOptions
                    {
                        // Only the accounts that have a confirmed email can sign-in
                        RequireConfirmedEmail = true
                    };
                    options.ClaimsIdentity.RoleClaimType = JwtClaimTypes.Role;
                })
                .AddEntityFrameworkStores<AuthDbContext>()
                .AddDefaultTokenProviders();

            var identityServerBuilder = services.AddIdentityServer(options =>
                {
                    options.Events.RaiseSuccessEvents = true;
                    options.Events.RaiseFailureEvents = true;
                })
                .AddConfigurationStore(option =>
                    option.ConfigureDbContext = builder =>
                        builder.UseNpgsql(connectionString, options => options.MigrationsAssembly("Nebula"))
                )
                .AddOperationalStore(option =>
                    option.ConfigureDbContext = builder =>
                        builder.UseNpgsql(connectionString, options => options.MigrationsAssembly("Nebula"))
                )
                .AddAspNetIdentity<ApplicationUser>()
                .AddProfileService<IdentityWithAdditionalClaimsProfileService>();

            var certificateFile = _configuration.GetValue<string>("IdentityServer:CertificateLocation");
            if (string.IsNullOrEmpty(certificateFile))
            {
                if (_environment.IsProduction())
                    _logger.LogCritical("No certificate was specified!");
                else
                    identityServerBuilder.AddDeveloperSigningCredential();
            }
            else
                identityServerBuilder.AddSigningCredential(new X509Certificate2(certificateFile));

            services.AddScoped<IEventSink, IdentityServerEventSink>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }
            app.UseHealthChecks("/health");

            app.UseCors("CorsPolicy");

            // Allows us to retrieve the request's IP address even when using a load balancer for example
            app.UseForwardedHeaders(new ForwardedHeadersOptions
            {
                ForwardedHeaders = ForwardedHeaders.XForwardedFor | ForwardedHeaders.XForwardedProto
            });

            app.UseHttpsRedirection();

            app.UseStaticFiles();

            app.UseIdentityServer();

            app.UseMvcWithDefaultRoute();
        }
    }
}
