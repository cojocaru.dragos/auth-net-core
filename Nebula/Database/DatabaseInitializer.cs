﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IdentityServer4.EntityFramework.DbContexts;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

namespace Nebula.Database
{
    public static class DatabaseInitializer
    {
        private static ConfigurationDbContext _configDbContext;
        private static AuthDbContext _authDbContext;
        private static UserManager<ApplicationUser> _userManager;
        private static RoleManager<ApplicationRole> _roleManager;

        public static async Task<IWebHost> InitializeDatabases(this IWebHost host)
        {
            using var serviceScope = host.Services.GetService<IServiceScopeFactory>().CreateScope();

            var persistedGrantDbContext = serviceScope.ServiceProvider.GetService<PersistedGrantDbContext>();
            await persistedGrantDbContext.Database.EnsureCreatedAsync();
            await persistedGrantDbContext.Database.MigrateAsync();

            _configDbContext = serviceScope.ServiceProvider.GetService<ConfigurationDbContext>();
            await _configDbContext.Database.EnsureCreatedAsync();
            await _configDbContext.Database.MigrateAsync();

            _authDbContext = serviceScope.ServiceProvider.GetService<AuthDbContext>();
            await _authDbContext.Database.EnsureCreatedAsync();
            await _authDbContext.Database.MigrateAsync();

            _roleManager = serviceScope.ServiceProvider.GetService<RoleManager<ApplicationRole>>();
            _userManager = serviceScope.ServiceProvider.GetService<UserManager<ApplicationUser>>();

            if (_authDbContext.Users.Any())
                return host;

            // Insert the admin role
            var adminRole = new ApplicationRole {Name = "admin", Description = "Full rights role"};
            if (!await _roleManager.RoleExistsAsync(adminRole.Name))
                await _roleManager.CreateAsync(adminRole);

            // Create the admin user
            var admin = new ApplicationUser
            {
                Email = "admin@admin.com",
                EmailConfirmed = true,
                UserName = "admin"
            };

            var hasher = new PasswordHasher<ApplicationUser>();
            admin.PasswordHash = hasher.HashPassword(admin, "P@ssw0rd!");

            // Insert the admin user
            await _userManager.CreateAsync(admin);

            // Set the admin role to the admin user
            await _userManager.AddToRoleAsync(admin, "admin");

            return host;
        }
    }
}
