﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;

namespace Nebula.Database
{
    public class ApplicationUser : IdentityUser<int>
    {
        public string RegistrationIpAddress { get; set; }

        public DateTime RegistrationDate { get; set; } = DateTime.UtcNow;

        public string LastIpAddress { get; set; }

        public DateTime? LastLogin { get; set; }
    }
}
